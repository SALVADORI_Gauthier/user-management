<?php
namespace DWWM\Controller;

use DWWM\Kernel\Router;
use DWWM\Kernel\SessionManager;

use DWWM\Model\Classes\Attribution;
use DWWM\Model\Classes\Groupe;
use DWWM\Model\Classes\Privilege;
use DWWM\View\View;

class AttributionController
{
    public static function updateAction()
    {
        if (count(SessionManager::hasPrivileges("attribution/read", true)) == 1)
        {
            $origin = Router::getOrigin();
            $path = Router::getPath();

            $user = SessionManager::getUser();
            $groupes = SessionManager::getGroupes();
            $privileges = SessionManager::getPrivileges();
            $isConnected = SessionManager::isConnected();

            $view_privileges = Privilege::getAll();
            $view_groupes = Groupe::getAll();
            $temp_attributions = Attribution::getAll();
            $view_attributions = [];
            foreach($temp_attributions as $attr)
            {
                $view_attributions[] = json_encode($attr);
            }
            
            $view = new View("attribution_update");
            $view->bindParam("origin", $origin);
            $view->bindParam("path", $path);
            $view->bindParam("user", $user);
            $view->bindParam("groupes", $groupes);
            $view->bindParam("privileges", $privileges);
            $view->bindParam("isConnected", $isConnected);
            $view->bindParam("view_privileges", $view_privileges);
            $view->bindParam("view_groupes", $view_groupes);
            $view->bindParam("view_attributions", $view_attributions);
            $view->display();
        }
        else
        {
            $origin = Router::getOrigin();
            $path = Router::getPath();
            $options = "/404";
            header("location: http://{$origin}{$path}{$options}");
        }
    }
    public static function submitUpdateAction()
    {
        unset($_POST['btnSubmit']);
        $keys = array_keys($_POST);
        $values = [];
        $options = ["options"=>["regexp"=>"/^[0-9]+_[0-9]+$/"]];

        foreach($_POST['attributions'] as $value)
        {
            $filtered = filter_var($value, FILTER_VALIDATE_REGEXP, $options);
            if (!empty($filtered))
            {
                $array = explode("_", $filtered);
                $id_groupe = $array[0];
                $id_privilege = $array[1];
                $values[] = 
                [
                    "id_groupe" => $id_groupe, 
                    "id_privilege" => $id_privilege
                ];
            }
        }
        
        Attribution::update($values);
        
        $origin = Router::getOrigin();
        $path = Router::getPath();
        $options = "/Attribution";
        header("location: http://{$origin}{$path}{$options}");
    }
}