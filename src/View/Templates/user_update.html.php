<?php
use DWWM\Kernel\SessionManager;
?>
<?php require "_head.html.php"; ?>
<?php require "_nav.html.php"; ?>
        <main role="main" class="container">
            <h1>DWWM - Session</h1>
            <h2>Edit User</h2>
<?php if($this->isConnected): ?>        
<?php if (count(SessionManager::hasPrivileges("utilisateur/update", true)) == 1): ?>
            <form method="post">
                <div>
                    <input type="hidden" name="id" value="<?= $this->edited_user->id; ?>">
                    login <input type="text" name="login" value="<?= $this->edited_user->login; ?>"><br>
                    <input type="submit" formaction="<?= $this->path; ?>/User/Update" name="btn-update" value="Mettre-à-jour">
                </div>
                <div style="display:table;">
                <div style="display:table-cell;">
                groupes affectés<br>
                <select name="affected_groupes" size="10" style="width:150px;">
<?php foreach($this->groupes_affectes as $groupe): ?>        
                    <option value="<?= $groupe->id; ?>"><?= $groupe->nom; ?></option>
<?php endforeach; ?>
                </select>
                </div>
                <div style="display:table-cell; padding:10px; vertical-align:middle;">
                    <p><button type="submit" formaction="<?= $this->path; ?>/Affect"> <= </button></p>
                    <p><button type="submit" formaction="<?= $this->path; ?>/Disaffect"> => </button></p>
                </div>
                <div style="display:table-cell;">
                groupes non-affectés<br>
                <select name="not_affected_groupes" size="10" style="width:150px;">
<?php foreach($this->groupes_non_affectes as $groupe): ?>        
                    <option value="<?= $groupe->id; ?>"><?= $groupe->nom; ?></option>
<?php endforeach; ?>
                </select>
                </div>
                </div>
            </form>
<?php endif; ?>
<?php endif; ?>
        </main>
<?php require "_body-end.html.php"; ?>