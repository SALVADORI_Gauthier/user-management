<?php
use DWWM\Kernel\SessionManager;
?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrateur(s)</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
<?php if (count(SessionManager::hasPrivileges("utilisateur", false)) > 0): ?>
                            <a class="dropdown-item" href="<?= $this->path; ?>/Users">Utilisateurs</a>
<?php endif; ?>
<?php if (count(SessionManager::hasPrivileges("attribution", false)) > 0): ?>
                            <a class="dropdown-item" href="<?= $this->path; ?>/Attribution">Groupes</a>
<?php endif; ?>
                        </div>
                    </li>
