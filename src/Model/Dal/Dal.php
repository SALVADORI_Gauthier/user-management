<?php

namespace DWWM\Model\Dal;

use \PDO;

abstract class Dal
{
    private $driver = "mysql";
    private $host = "127.0.0.1";
    private $port = "3306";
    private $user = "dwwm_session";
    private $password = "fjsgfKgq0bn9UOxY";
    private $database = "dwwm_session";
    private $charset = "utf8";
    private $dsn;

    public function __construct()
    {
        $this->dsn = "{$this->driver}:";
        $this->dsn .= "host={$this->host};";
        $this->dsn .= "port={$this->port};";
        $this->dsn .= "dbname={$this->database};";
        $this->dsn .= "charset={$this->charset};";
    }

    public function open()
    {
        return new PDO($this->dsn, $this->user, $this->password, []);
    }
} 
